# DEV FULLSTACK CHALLENGE

### Obiettivo

Dimostrare le tue competenze di programmazione frontend e backend.

### Task

Creare una semplice mappa che riporti gli immobili presenti nel file allegato.
L'immobile dovrà presentarsi come marker sulla mappa e, cliccando sopra, apparirà (popup) il dettaglio dell'immobile (prezzo, stanze, bagni, indirizzo, etc...). La mappa dovrà adattarsi alle diverse misure della finestra, pertanto ci aspettiamo che la mappa sia responsiva in ottica Mobile First.

Il file properties.json contiene dati di esempio degli immobili.

Assumi che la base dati sia dinamica e che i dati arrivino da un database, il json è usato solo per semplicità di implementazione.
Crea delle API che dovranno essere consumate dal frontend.
-Implementa una semplice ricerca basata sull'indirizzo dell'immobile, il risultato dovrà essere visualizzato come marker sulla mappa.

Aggiungi il codice, i test e la documentazione che ritieni opportuna
(dockblock, commenti, README).

Se qualche requisito non ti è chiaro sentiti libero di improvvisare.

Puoi utilizzare framework o librerie già pronte se lo ritieni opportuno. Se utilizzi codice di terze parti ti consigliamo di utilizzare npm/yarn.
Ti consigliamo l'utilizzo di Typescript per la realizzazione di entrambi gli ambienti.

### Consegna test

Il test dovrà essere pubblicato sul tuo repository (pubblico o privato che sia)
GitHub, GitLab, BitBucket etc.

### Ambiente di Sviluppo

Ti inviamo ad usare un'immagine Docker in base al linguaggio scelto. Per semplicità
puoi usare anche Docker Compose o Docker Swarm.

### Valutazione

Il task sarà valutato sulla base del tuo utilizzo della programmazione (leggibilità e manutenibilità del codice) e sulla UX e UI.
In tal senso, la copertura con test automatici è incoraggiata.

Ti invitiamo a curare tutti gli aspetti come se si trattasse di un progetto pronto per andare in produzione. Non consegnare file o codice inutilizzato, soprattutto se proveniente da framework di terze parti.

### Alternativa linguaggio (Javascript o PHP)

In alternativa, se ti trovi più a tuo agio, puoi usare come linguaggio PHP.

Anche (e soprattutto) in questo caso è indispensabile indicare nel README tutti i
passaggi necessari all'installazione e verifica.
