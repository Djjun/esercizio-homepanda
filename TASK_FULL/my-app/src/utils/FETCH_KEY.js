const API_PATH = "http://localhost:5003/api";

export const FETCH_KEY = {
  GET_PROPERTIES: `${API_PATH}/properties`,
  GET_PROPERTIES_FILTERED: `${API_PATH}/properties/:address`,
};
