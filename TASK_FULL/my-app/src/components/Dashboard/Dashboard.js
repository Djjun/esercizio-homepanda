import React from "react";
import { StyledDashboard } from "./Dashboard.styled.js";
import Map from "../Map/Map.js";

const Dashboard = () => {
  return (
    <StyledDashboard>
      <Map />
    </StyledDashboard>
  );
};

export default Dashboard;
