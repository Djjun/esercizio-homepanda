import React from "react";
import MarkerPopup from "../MarkerPopup/MarkerPopup";
import { Marker } from "react-leaflet";
import L from "leaflet";
import PropTypes from "prop-types";

/**
 * I need to delete the default Icon because It makes 404 error
 */
delete L.Icon.Default.prototype._getIconUrl;

/**
 * Set default Icon
 */
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

const MapMarker = ({ markerProps }) => {
  const { latitude, longitude, ...props } = markerProps;
  return (
    <Marker title={props.title} position={[latitude, longitude]}>
      <MarkerPopup popupProps={props} />
    </Marker>
  );
};

MapMarker.prototype = {
  markerProps: PropTypes.arrayOf(PropTypes.object),
};

MapMarker.defaultProps = {
  markerProps: [],
};

export default MapMarker;
