import React from "react";
import { Popup } from "react-leaflet";
import { Box, CardMedia, Typography } from "@mui/material";
import "./MarkerPopup.css";
import PropTypes from "prop-types";

const MarkerPopup = ({ popupProps }) => {
  const {
    images,
    title,
    typology,
    baths,
    energy_class,
    floor,
    hasElevator,
    rooms,
    surface,
    address,
    province,
    city,
    price,
  } = popupProps;
  return (
    <Popup maxWidth={350}>
      <Box>
        <CardMedia component="img" image={images} />
        <Box sx={{ display: "flex", flexDirection: "column", padding: 2 }}>
          <Typography component="div" variant="h6">
            {title}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {typology}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`baths: ${baths}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`energy class: ${energy_class}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`floor: ${floor}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`Has elevator: ${hasElevator ? "yes" : "no"}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`rooms: ${rooms}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`surface: ${surface}m²`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`address: ${address}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`province: ${province}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`city: ${city}`}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
          >
            {`price: ${price}€`}
          </Typography>
        </Box>
      </Box>
    </Popup>
  );
};

MarkerPopup.prototype = {
  images: PropTypes.string,
  title: PropTypes.string,
  typology: PropTypes.string,
  baths: PropTypes.number,
  energy_class: PropTypes.string,
  floor: PropTypes.string,
  hasElevator: PropTypes.number,
  rooms: PropTypes.number,
  surface: PropTypes.number,
  address: PropTypes.string,
  province: PropTypes.string,
  city: PropTypes.string,
  price: PropTypes.number,
};

MarkerPopup.defaultProps = {
  images: "",
  title: "",
  typology: "",
  baths: 0,
  energy_class: "",
  floor: "",
  hasElevator: 0,
  rooms: 0,
  surface: 0,
  address: "",
  province: "",
  city: "",
  price: 0,
};

export default MarkerPopup;
