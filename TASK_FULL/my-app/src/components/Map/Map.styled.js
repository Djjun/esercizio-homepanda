import styled from "styled-components";

export const ContentOverlay = styled.div`
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  row-gap: 1rem;
  pointer-events: none;
`;

export const FiltersContainer = styled.div`
  pointer-events: initial;
  position: relative;
  display: inline-flex;
  align-items: center;
  padding: 0.2rem 0.75rem;
  border-radius: 0.5rem;
  column-gap: 0.5rem;
`;

export const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const TopContent = styled.div`
  display: flex;
  justify-content: center;
`;
