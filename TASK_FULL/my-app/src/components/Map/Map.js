import React, { useEffect, useMemo, useState } from "react";
import { MapContainer, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import MapMarker from "../MapMarker/MapMarker";
import axios from "axios";
import { FETCH_KEY } from "../../utils/FETCH_KEY";
import { InputAdornment, TextField } from "@mui/material";
import {
  Container,
  ContentOverlay,
  FiltersContainer,
  TopContent,
} from "./Map.styled";
import SearchIcon from "@mui/icons-material/Search";

const Map = () => {
  const position = [51.505, -0.09];
  const [searchValue, setSearchValue] = useState("");
  const [markers, setMarkers] = useState([]);

  const filteredMarkers = useMemo(
    () =>
      markers.filter((item) =>
        item?.address?.toLowerCase().includes(searchValue.toLowerCase())
      ),
    [markers, searchValue]
  );

  useEffect(() => {
    axios
      .get(FETCH_KEY.GET_PROPERTIES)
      .then((res) => setMarkers(res.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <Container>
      <MapContainer
        center={position}
        zoom={3}
        scrollWheelZoom={false}
        style={{
          height: "100vh",
          width: "100vw",
          position: "absolute",
          zIndex: 1,
        }}
        worldCopyJump={true}
        minZoom={3}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          noWrap={true}
        />

        {filteredMarkers?.map((marker, idx) => (
          <MapMarker key={idx} markerProps={marker} />
        ))}
      </MapContainer>
      <ContentOverlay>
        <TopContent>
          <FiltersContainer>
            <TextField
              sx={{ backgroundColor: "#FFF", width: "400px" }}
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </FiltersContainer>
        </TopContent>
      </ContentOverlay>
    </Container>
  );
};

export default Map;
