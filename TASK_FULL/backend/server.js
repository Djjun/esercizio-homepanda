const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const app = express();

dotenv.config();

const properties = require(process.env.PROPERTIES_PATH);

app.use(cors());

app.get("/", (req, res) => {
  res.send("API is running..");
});

/**
 * Get all properties from file json
 */
app.get("/api/properties", (req, res) => {
  res.json(properties);
});

/**
 * Get properties filtered from address
 */
app.get("/api/properties/:address", (req, res) => {
  const properties = properties.find((n) => n.address === req.params.address);
  res.send(properties);
});

const PORT = process.env.PORT || 5003;

app.listen(PORT, console.log(`Server started on PORT ${PORT}`));
